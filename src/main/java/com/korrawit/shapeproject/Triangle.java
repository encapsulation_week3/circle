/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.shapeproject;

/**
 *
 * @author DELL
 */
public class Triangle {

    private double b;
    private double h;

    public Triangle(double b, double h) {
        this.b = b;
        this.h = h;
    }

    public double calAreaTriangle() {
        return 0.5 * b * h;
    }

    public double getB() {
        return b;
    }

    public double getH() {
        return h;
    }

    public void setBAndH(double b, double h) {
        if (b <= 0 || h <= 0) {
            System.out.println("Error : Base and High must more then 0!!!");;
        }
        this.b = b;
        this.h = h;

    }

}
