/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.shapeproject;

/**
 *
 * @author DELL
 */
public class Rectengle {

    private double h;
    private double w;

    public Rectengle(double h, double w) {
        this.h = h;
        this.w = w;
    }

    public double calAreaRectangle() {
        return h * w;
    }

    public double getH() {
        return h;
    }

    public double getW() {
        return w;
    }

    public void setHAndW(double h, double w) {
        if (h <= 0 || w <= 0) {
            System.out.println("Error : High and Width must more then 0!!!");;
        }
        this.h = h;
        this.w = w;

    }

}
