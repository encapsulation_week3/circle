/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.shapeproject;

/**
 *
 * @author DELL
 */
public class Squre {

    private double s;

    public Squre(double s) {
        this.s = s;
    }

    public double calAreaSqure() {
        return s * s;
    }

    public double getSide() {
        return s;
    }

    public void setSide(double s) {
        if (s == 0) {
            System.out.println("Error : Side must more then 0!!!");;
        }
        if (s < 0) {
            System.out.println("Error : Side must not be negative!!!");;
        }
        this.s = s;

    }

}
